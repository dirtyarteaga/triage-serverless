# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/job/keep_cache_warm_job'

RSpec.describe Triage::KeepCacheWarmJob do
  subject { described_class.new }

  describe '#perform' do
    it 'refreshes cache we are using' do
      allow(subject).to receive(:puts)

      expect(Triage).to receive(:gitlab_org_group_member_ids).with(fresh: true)
      expect(Triage).to receive(:gitlab_com_group_member_ids).with(fresh: true)
      expect(Triage).to receive(:gitlab_core_team_community_members_group_member_ids).with(fresh: true)
      expect(Triage).to receive(:gitlab_org_group_member_usernames).with(fresh: true)
      expect(Triage).to receive(:gitlab_com_group_member_usernames).with(fresh: true)
      expect(Triage).to receive(:gitlab_core_team_community_members_group_member_usernames).with(fresh: true)
      expect(Triage).to receive(:jihu_team_member_ids).with(fresh: true)

      subject.perform
    end
  end
end
