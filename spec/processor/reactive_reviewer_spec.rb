# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/reactive_reviewer'

RSpec.describe Triage::ReactiveReviewer do
  include_context 'with event', 'Triage::NoteEvent' do
    let(:event_attrs) do
      {
        object_kind: 'note',
        noteable_type: 'merge_request',
        from_gitlab_org?: true,
        jihu_contributor?: true,
        by_noteable_author?: true,
        note?: true,
        new_entity?: false,
        noteable_author_id: 1,
        noteable_path: '/foo',
        new_comment: new_comment
      }
    end

    let(:new_comment) { %(#{Triage::GITLAB_BOT} assign_reviewer @gitlab-reviewer) }
  end

  before do
    allow(Triage).to receive(:gitlab_org_group_member_usernames).and_return(%w[gitlab-reviewer gl-reviewer1 gl-reviewer2])
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.note']

  describe '#applicable?' do
    context 'when event project is not under gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when merge request is not by a JiHu contributor' do
      before do
        allow(event).to receive(:jihu_contributor?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when merge request note is not by the noteable author' do
      before do
        allow(event).to receive(:by_noteable_author?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when event is not mentioning the bot' do
      let(:new_comment) { '@foo assign_reviewer @me' }

      include_examples 'event is not applicable'
    end

    context 'when event is not using the "assign_reviewer" action' do
      let(:new_comment) { "#{Triage::GITLAB_BOT} label ~bar" }

      include_examples 'event is not applicable'
    end

    context 'when event is assigning to a gitlab reviewer' do
      let(:new_comment) { "#{Triage::GITLAB_BOT} assign_reviewer @gitlab-reviewer" }

      include_examples 'event is applicable'
    end

    context 'when event is not assigning to allowed gitlab reviewer' do
      let(:new_comment) { "#{Triage::GITLAB_BOT} assign_reviewer @not-gl-reviewer" }

      include_examples 'event is not applicable'
    end
  end

  describe '#process' do
    shared_examples 'message posting' do |expected_reviewers:|
      it 'posts /assign_reviewer command' do
        body = <<~MARKDOWN.chomp
          /assign_reviewer #{Array(expected_reviewers).join(' ')}
        MARKDOWN

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    context 'when reviewer is @gitlab-reviewer' do
      let(:new_comment) { "#{Triage::GITLAB_BOT} assign_reviewer @gitlab-reviewer" }
      it_behaves_like 'message posting', expected_reviewers: '@gitlab-reviewer'
    end

    context 'when command is not at the beginning of the comment' do
      let(:new_comment) { "Hello\n#{Triage::GITLAB_BOT} assign_reviewer @gitlab-reviewer\nWorld!" }

      it_behaves_like 'message posting', expected_reviewers: '@gitlab-reviewer'
    end

    context 'with multiple reviewers on the same line with extra spaces' do
      let(:new_comment) { "#{Triage::GITLAB_BOT} assign_reviewer   @gl-reviewer1    @gl-reviewer2" }

      it_behaves_like 'message posting', expected_reviewers: %w[@gl-reviewer1 @gl-reviewer2]
    end

    context 'with multiple commands on separate lines' do
      let(:new_comment) do
        <<~COMMENT
          Hello
          #{Triage::GITLAB_BOT} assign_reviewer @gl-reviewer1 
          #{Triage::GITLAB_BOT} assign_reviewer @gl-reviewer2
        COMMENT
      end

      it_behaves_like 'message posting', expected_reviewers: %w[@gl-reviewer1 @gl-reviewer2]
    end

    context 'with a mix of allowed and disallowed reviewers' do
      let(:new_comment) do
        <<~COMMENT
          Hello
          #{Triage::GITLAB_BOT} assign_reviewer @gl-reviewer1 
          #{Triage::GITLAB_BOT} assign_reviewer @not-gl-reviewer
        COMMENT
      end

      it_behaves_like 'message posting', expected_reviewers: %w[@gl-reviewer1]
    end
  end

  it_behaves_like 'rate limited'
end
