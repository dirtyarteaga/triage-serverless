# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/merge_request_help'

RSpec.describe Triage::MergeRequestHelp do
  include_context 'with event', 'Triage::NoteEvent' do
    let(:event_attrs) do
      {
        object_kind: 'note',
        noteable_type: 'merge_request',
        from_gitlab_org?: true,
        wider_community_author?: true,
        by_noteable_author?: true,
        note?: true,
        noteable_author_id: 1,
        noteable_path: '/foo',
        new_comment: %(@gitlab-bot help),
        url: 'https://comment.url/note#123'
      }
    end
  end

  let(:messenger) { double('messenger', ping: true) }

  subject { described_class.new(event, messenger: messenger) }

  include_examples 'registers listeners', ['merge_request.note']

  describe 'ACTION_REGEXP' do
    it { expect(described_class::ACTION_REGEXP).to eq(/^#{Regexp.escape(Triage::GITLAB_BOT)}[[:space:]]+help/) }
  end

  describe '#applicable?' do
    context 'when event is from gitlab org, for a note on a new merge request, by the mr author' do
      include_examples 'event is applicable'
    end

    context 'when event project is not under gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when event is not mentioning the bot' do
      before do
        allow(event).to receive(:new_comment).and_return('@foo label ~bar')
      end

      include_examples 'event is not applicable'
    end

    context 'when event is not using the "help" action' do
      before do
        allow(event).to receive(:new_comment).and_return("@gitlab-bot label ~foo")
      end

      include_examples 'event is not applicable'
    end

    context 'when event author is not the noteable author' do
      before do
        allow(event).to receive(:by_noteable_author?).and_return(false)
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#process' do
    it 'posts a message to slack and a comment to reply to author' do
      slack_message = "Hi MR coaches, a contributor has requested help in #{event.url}.\n"
      expect(messenger).to receive(:ping).with(slack_message)

      body = <<~MARKDOWN.chomp
        :wave: @root,

        Thanks for reaching out for help. I've notified [the merge request coaches](https://about.gitlab.com/company/team/?department=merge-request-coach)

        They will get back to you as soon as they can.

        If you have not received any response, you may ask for help again after 1 day.
      MARKDOWN

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end

    it_behaves_like 'rate limited', count: 1, period: 86400
  end
end
