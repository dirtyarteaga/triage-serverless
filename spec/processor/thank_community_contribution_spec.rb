# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/thank_community_contribution'
require_relative '../../triage/triage/event'

RSpec.describe Triage::ThankCommunityContribution do
  include_context 'with event', 'Triage::MergeRequestEvent' do
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'open',
        from_gitlab_org?: true,
        wider_community_author?: true,
        user_username: 'root',
        noteable_path: '/foo',
        project_id: project_id
      }
    end
    let(:project_id) { 13_579 }
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.open']

  describe '#applicable?' do
    context 'when event is for a new merge request opened by a wider community author under the gitlab-org group' do
      include_examples 'event is applicable'
    end

    context 'when event project is not under gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when event is not from a wider community author' do
      before do
        allow(event).to receive(:wider_community_author?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'for project with custom conditions' do
      let(:project_id) { described_class::WWW_GITLAB_COM_PROJECT_ID }

      before do
        allow(event).to receive(:from_gitlab_com?).and_return(false)
      end

      it 'processes different conditions' do
        expect(event).to receive(:from_gitlab_com?)
        subject.applicable?
      end
    end
  end

  describe '#process' do
    let(:expected_message_template) { Triage::Strings::Thanks::DEFAULT_THANKS }
    let(:expected_message) do format(expected_message_template, author_username: event_attrs[:user_username])
    end

    it 'posts a default message' do
      expect_comment_request(event: event, body: expected_message) do
        subject.process
      end
    end

    context 'when project_id has override thanks' do
      let(:expected_message_template) { described_class::PROJECT_THANKS[project_id][:message] }

      context 'GitLab' do
        let(:project_id) { 278_964 }
        let(:expected_message) do
          <<~MARKDOWN.chomp
            :wave: @root

            Thank you for your contribution to GitLab. We believe that [everyone can contribute](https://about.gitlab.com/company/mission/#mission)
            and contributions like yours are what make GitLab great!

            * Our [Merge Request Coaches](https://about.gitlab.com/company/team/?department=merge-request-coach)
            will ensure your contribution is reviewed in a timely manner[*](https://about.gitlab.com/handbook/engineering/quality/merge-request-triage).
            * If you haven't, please set up a [`DANGER_GITLAB_API_TOKEN`](https://docs.gitlab.com/ee/development/dangerbot.html#limitations).
            * You can comment `@gitlab-bot label ~"group::"` to add a [group label](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#group-labels).
            * After a few days, feel free to ask `@gitlab-bot help` or [ping a Merge Request Coach](https://about.gitlab.com/company/team/?department=merge-request-coach).
            * Read more on [how to get help](https://about.gitlab.com/community/contribute/#getting-help).

            *This message was [generated automatically](https://about.gitlab.com/handbook/engineering/quality/triage-operations/#ensure-quick-feedback-for-community-contributions).
            You're welcome to [improve it](https://gitlab.com/gitlab-org/quality/triage-serverless/-/blob/master/triage/strings/thanks.rb).*

            /label ~"Community contribution"
          MARKDOWN
        end

        it 'posts custom project comment' do
          expect_comment_request(event: event, body: expected_message) do
            subject.process
          end
        end
      end

      context 'runner' do
        let(:project_id) { 250_833 }

        it 'posts custom project comment' do
          expect_comment_request(event: event, body: expected_message) do
            subject.process
          end
        end
      end

      context 'website' do
        let(:project_id) { 7764 }

        it 'posts custom project comment' do
          expect_comment_request(event: event, body: expected_message) do
            subject.process
          end
        end
      end
    end
  end
end
