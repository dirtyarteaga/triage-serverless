RSpec.shared_examples 'registers listeners' do |listeners|
  let(:processor) { described_class }

  describe '.listeners' do
    it 'processes different conditions' do
      expect(processor.listeners.map(&:event)).to match_array(listeners)
    end
  end
end
