
# Overview

This project runs a service which listens to webhooks from [gitlab-org group](https://gitlab.com/gitlab-org),
and it reacts upon webhooks events. So far it runs:

* AvailabilityPriority (when label added)
  * This ensures availability issues have minimal priorities based on severity.
    For example, for `~severity::1` and `~severity::2` availability issues, the minimal
    priority is `~priority::1`.
* BackstageLabel (when label added)
  * This hints and removes `~"backstage [DEPRECATED]"` label because we don't
    want to use this label anymore.
* CustomerLabel (when comment added)
  * This adds a `~customer` label whenever the comment contains a link to a
    customer support ticket link.
* TypeLabel (when label added)
  * This adds a type label whenever a subtype label is added.

# Development

## Install gems

```
bundle install
```

## Run tests

You can run the test suite with `bundle exec rspec`.

Guard is also supported (see the [`Guardfile`](Guardfile)). Install Guard with `gem install guard`, then start it with `guard`.

## Running as a local server

* The steps to run this project as a local server is based on [Serverless Documentations](https://docs.gitlab.com/ee/user/project/clusters/serverless/#running-functions-locally)

* In the project directory, build the docker image. This creates a docker image named `triage-reactive`:
```
docker build --file=Dockerfile.rack -t triage-reactive .
```

* Run the server locally (here, in dry mode) using docker, passing required environment variables.

```
docker run -itp 8080:8080 \
  -e GITLAB_API_ENDPOINT=http://localhost:3000/api/v4 \
  -e GITLAB_WEBHOOK_TOKEN=gitlab_webhook_token \
  -e GITLAB_API_TOKEN=gitlab_api_token \
  -e SLACK_WEBHOOK_URL=https://example.org \
  -e DRY_RUN=1 \
  triage-reactive
```

* Test the endpoint

```
curl -X POST 0.0.0.0:8080 -H "Content-Type: application/json" -H "X-Gitlab-Token: gitlab_webhook_token" -d @spec/fixture/gitlab_test_note.json
```

## Running with a local GitLab instance

You can run `triage-serverless` with a local GitLab instance (e.g GDK) and have it react to events coming from the local instance.

There are some prerequisites you would need in the local GitLab instance:
1. The following groups and subgroups:
    - `gitlab-org`
    - `gitlab-com`
    - `gitlab-org/gitlab-core-team/community-members`
1. The following projects:
    - `gitlab-org/gitlab`
1. A `bot` user who is a member of `gitlab-org` and `gitlab-com` groups, with an api scoped access token. This access token should be set in the environment `GITLAB_API_TOKEN` when running `triage-serverless`.
1. Enable outbound webhook request to `localhost`. See [documentation](https://docs.gitlab.com/ee/security/webhooks.html) for detail. 

To start:
1. Start `triage-serverless` locally with the following environment variables: `GITLAB_API_ENDPOINT=http://localhost:3000/api/v4 GITLAB_WEBHOOK_TOKEN=gitlab_webhook_token GITLAB_API_TOKEN=gitlab_api_token bundle exec rackup -p 4567`
1. In the local GitLab's `gitlab-org/gitlab` project, create a webhook integration to the `triage-serverless` address (e.g `http://localhost:4567`) with the webhook token above.

TODO:
- [ ] Stub slack webhook URL

## Implementing a new Processor

To implement a new processor that responds to a specific event:
1. Create a new subclass of `Processor` in `triage/processor/`
1. Implement the following methods:
    1. `#applicable?` - a method that returns `true` or `false` on whether the event is applicable to this processor.
    1. `#process` - a method that processes the event and performs what is needed.
1. Add the new processor to the `DEFAULT_PROCESSORS` in `triage/triage/handler.rb`

# Production

## How we run on Kubernetes

The Kubernetes cluster is in the project
[gitlab-qa-resources](https://console.cloud.google.com/kubernetes/list?project=gitlab-qa-resources).

At the moment, we're not running on Serverless, but an ordinary web server to
process the webhooks.

0. <https://triage-serverless.gitlab.com/> Resolve [DNS records](https://ops.gitlab.net/gitlab-com/gitlab-com-infrastructure/-/merge_requests/1945) to IP pointing to `ingress-nginx-ingress-controller` under `gitlab-managed-apps`:

       kubectl describe service ingress-nginx-ingress-controller --namespace=gitlab-managed-apps

0. Then where the traffic goes?
0. Then traffic goes to `triage-web-ingress`

       kubectl describe ingress triage-web-ingress --namespace=triage-serverless-12690061-production-2

0. Then traffic goes to `triage-web-service`

       kubectl describe service triage-web-service --namespace=triage-serverless-12690061-production-2

0. And lastly traffic goes to `triage-web`

       kubectl describe deployment triage-web --namespace=triage-serverless-12690061-production-2

# `event.from_gitlab_org?` as feature flags

We can use `event.from_gitlab_org?` to block events from
[`gitlab-org`](https://gitlab.com/gitlab-org), and deploy to production,
and then test in
[triage-test](https://gitlab.com/gl-quality/eng-prod/triage-test).
This way, we can try out the new feature in a separate project before
applying it to `gitlab-org`.

After enough tests, we can then create a merge request and remove the block,
actually applying to `gitlab-org`.

## Setting up `kubectl` for production access

* Install `gcloud`

      brew cask install google-cloud-sdk

* Install `kubectl`

      brew install kubernetes-cli

* Log in to GCK

      gcloud auth login

* Configure `kubectl`

      gcloud container clusters get-credentials triage-serverless-prod --zone us-central1 --project gitlab-qa-resources

* Run local proxy

      kubectl proxy
