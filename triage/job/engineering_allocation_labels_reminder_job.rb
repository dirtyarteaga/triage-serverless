# frozen_string_literal: true

require_relative '../triage'
require_relative '../triage/reaction'

require 'sucker_punch'

module Triage
  class EngineeringAllocationLabelsReminderJob
    include SuckerPunch::Job
    include Reaction

    PRODUCER_LABEL_PREFIX = 'Eng-Producer::'
    CONSUMER_LABEL_PREFIX = 'Eng-Consumer::'
    PRIORITY_LABEL_PREFIX = 'priority::'
    SEVERITY_LABEL_PREFIX = 'severity::'
    BUG_LABEL = 'bug'

    attr_reader :event

    def perform(event)
      SuckerPunch.logger.info("Performing job #{self.class.name} - Issue URL: #{event.url}")

      @event = event

      return if issue_has_required_labels?

      post_reminder
    end

    private

    def issue_has_required_labels?
      checks = [
        :has_producer_label?,
        :has_consumer_label?,
        :has_priority_label?,
        :has_severity_label?
      ]

      checks.all? { |check| method(check).call(issue.labels) }
    end

    def issue
      @issue ||= Triage.api_client.issue(event.project_id, event.iid)
    end

    def has_producer_label?(labels)
      labels.any? { |label| label.start_with?(PRODUCER_LABEL_PREFIX) }
    end

    def has_consumer_label?(labels)
      labels.any? { |label| label.start_with?(CONSUMER_LABEL_PREFIX) }
    end

    def has_priority_label?(labels)
      labels.any? { |label| label.start_with?(PRIORITY_LABEL_PREFIX) }
    end

    def has_severity_label?(labels)
      !labels.include?(BUG_LABEL) || labels.any? { |label| label.start_with?(SEVERITY_LABEL_PREFIX) }
    end

    def post_reminder
      message = <<~MARKDOWN.chomp
        :wave: `@#{event.user_username}`, please ensure the following labels are present for ~"Engineering Allocation":

        - An `~Eng-Consumer::*` label
        - An `~Eng-Producer::*` label
        - A `~priority::*` label
        - A `~severity::*` label when the type is ~"bug"
      MARKDOWN

      add_comment(message)
    end
  end
end
