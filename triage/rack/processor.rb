require 'rack'

require_relative '../triage/error'
require_relative '../triage/handler'
require_relative '../triage/sentry'
require_relative '../triage'

module Triage
  module Rack
    class Processor
      def call(env)
        event = env[:event]
        handler = Handler.new(event)

        puts "Running in dry mode" if Triage.dry_run?

        results = handler.process

        puts "Incoming webhook event: #{event.inspect}, response: #{results.inspect}"

        capture_errors(results, event)

        messages = results.transform_values(&:message).compact

        ::Rack::Response.new([JSON.dump(status: :ok, messages: messages)]).finish

      rescue Triage::ClientError => error
        error_response(error)
      rescue => error
        capture_error(nil, error, event)
        error_response(error, status: 500)
      end

      private

      def error_response(error, status: 400)
        ::Rack::Response.new([JSON.dump(status: :error, error: error.class, message: error.message)], status).finish
      end

      def capture_errors(results, event)
        results.each do |processor, result|
          next unless result.error

          capture_error(processor, result.error, event)
        end
      end

      def capture_error(processor, error, event)
        Raven.tags_context(object_kind: event.class.to_s)
        Raven.extra_context(processor: processor, payload: event.payload)
        Raven.capture_exception(error)

        puts "Exception for processor: #{processor.inspect} with event: #{event.inspect}, error: #{error.class}: #{error.message}"
      end

      def dry_run?
        !ENV['DRY_RUN'].nil?
      end
    end
  end
end
