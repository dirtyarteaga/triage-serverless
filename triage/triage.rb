# frozen_string_literal: true

require 'mini_cache'
require 'gitlab'

module Triage
  PRODUCTION_API_ENDPOINT = ENV['GITLAB_API_ENDPOINT'] || 'https://gitlab.com/api/v4'
  GITLAB_BOT = '@gitlab-bot'
  GITLAB_ORG_GROUP = 'gitlab-org'
  GITLAB_COM_GROUP = 'gitlab-com'
  GITLAB_CORE_TEAM_COMMUNITY_MEMBERS_GROUP = 'gitlab-org/gitlab-core-team/community-members'
  JIHU_TEAM_MEMBER_GROUP = 'gitlab-jh/jh-team'
  GROUP_CACHE_DEFAULT_EXPIRATION = 3600 # 1 hour
  GROUP_CACHE = {
    GITLAB_ORG_GROUP => [
      {
        name: :gitlab_org_group_member_ids,
        attr: :id,
      },
      {
        name: :gitlab_org_group_member_usernames,
        attr: :username,
      }
    ],
    GITLAB_COM_GROUP => [
      {
        name: :gitlab_com_group_member_ids,
        attr: :id,
      },
      {
        name: :gitlab_com_group_member_usernames,
        attr: :username,
      }
    ],
    GITLAB_CORE_TEAM_COMMUNITY_MEMBERS_GROUP => [
      {
        name: :gitlab_core_team_community_members_group_member_ids,
        attr: :id,
      },
      {
        name: :gitlab_core_team_community_members_group_member_usernames,
        attr: :username,
      }
    ],
    JIHU_TEAM_MEMBER_GROUP => [
      {
        name: :jihu_team_member_ids,
        attr: :id,
      }
    ]
  }.freeze
  GROUP_MEMBER_ATTRS_CACHEABLE = %i[id username].freeze

  def self.dry_run?
    !ENV['DRY_RUN'].nil?
  end

  def self.cache
    @cache ||= MiniCache::Store.new
  end

  def self.api_client
    cache.get_or_set(:api_client) do
      Gitlab.client(endpoint: PRODUCTION_API_ENDPOINT, private_token: ENV['GITLAB_API_TOKEN'].to_s)
    end
  end

  class << self
    GROUP_CACHE.each do |group, cache_definitions|
      cache_definitions.each do |definition|
        define_method(definition.fetch(:name)) do |fresh: false|
          group_member_attrs(
            group,
            definition.fetch(:attr),
            fresh: fresh,
            expires_in: definition.fetch(:expires_in, GROUP_CACHE_DEFAULT_EXPIRATION))
        end
      end
    end
  end

  def self.group_member_attrs(group, attr, fresh:, expires_in:)
    cache_method = fresh ? :set : :get_or_set
    # Make sure we have the up-to-date source data
    cached_group_members = group_members(group, fresh: fresh, expires_in: expires_in)

    cache.public_send(cache_method, "#{group}_group_member_#{attr}s".to_sym) do
      member_attrs = cached_group_members.map(&attr)
      # Don't set an expiration time here as these caches are cleared in `.group_members`.
      MiniCache::Data.new(member_attrs)
    end
  end
  private_class_method :group_member_attrs

  def self.group_members(group, fresh:, expires_in:)
    cache_method = fresh ? :set : :get_or_set

    cache.public_send(cache_method, "#{group}_group_members".to_sym) do
      MiniCache::Data.new(fresh_group_members(group), expires_in: expires_in).tap do
        group_member_attrs_cache_keys(group).each { |cache_key| cache.unset(cache_key) }
      end
    end
  end
  private_class_method :group_members

  def self.group_member_attrs_cache_keys(group)
    GROUP_MEMBER_ATTRS_CACHEABLE.map { |attr| "#{group}_group_member_#{attr}s".to_sym }
  end

  def self.fresh_group_members(group)
    api_client
      .group_members(group, per_page: 100)
      .auto_paginate
  end
  private_class_method :fresh_group_members
end
