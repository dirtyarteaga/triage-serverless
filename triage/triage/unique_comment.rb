# frozen_string_literal: true

module Triage
  class UniqueComment
    NOTES_PER_PAGE = 100

    def initialize(class_name, event)
      @class_name = class_name
      @event = event
    end

    def no_previous_comment?
      resource_notes.none? do |note|
        note.body.start_with?(hidden_comment)
      end
    end

    def wrap(message)
      "#{hidden_comment}\n#{message}"
    end

    private

    attr_reader :class_name, :event

    def hidden_comment
      "<!-- triage-serverless #{class_name.split('::').last} -->"
    end

    def resource_notes
      Triage.api_client.public_send("#{event.object_kind}_notes", project_id, resource_iid, per_page: NOTES_PER_PAGE).auto_paginate
    end

    def resource_iid
      event.iid
    end

    def project_id
      event.project_id
    end
  end
end
