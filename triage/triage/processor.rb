# frozen_string_literal: true

require 'gitlab'

require_relative 'listener'
require_relative 'reaction'

module Triage
  class Processor
    include Reaction

    attr_reader :event

    def self.react_to(*events_def)
      events_def.each do |event_def|
        listener = Listener.listeners_for_event(*event_def.split('.'))
        self.listeners.concat(listener)
      end
    end

    def self.listeners
      @listeners ||= []
    end

    def self.triage(event)
      new(event).triage
    end

    def initialize(event)
      @event = event
    end

    def triage
      return unless applicable?

      before_process
      process.tap { after_process }
    end

    def process
      raise NotImplementedError
    end

    private

    def applicable?
      true
    end

    def before_process
      nil
    end

    def after_process
      nil
    end
  end
end
