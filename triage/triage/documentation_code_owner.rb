# frozen_string_literal: true

module Triage
  class DocumentationCodeOwner
    DOC_SECTION_PATTERN = %r{\ADocumentation|\ADocs}
    RULE_TYPE_CODE_OWNER = 'code_owner'

    def initialize(project_id, merge_request_iid)
      @project_id = project_id
      @merge_request_iid = merge_request_iid
    end

    def approvers
      return docs_approvers_by_type[:users] unless docs_approvers_by_type[:users].empty?

      docs_approvers_by_type[:groups]
    end

    private

    attr_reader :project_id, :merge_request_iid

    def docs_approvers_by_type
      return @docs_approvers_by_type if @docs_approvers_by_type

      doc_approval_rules = approval_rules.select do |rule|
        rule.rule_type == RULE_TYPE_CODE_OWNER && rule.section&.match(DOC_SECTION_PATTERN)
      end

      @docs_approvers_by_type = doc_approval_rules.each_with_object({ users: Set.new, groups: Set.new }) do |approval_rule, approvers|
        approval_rule.users.each { |user| approvers[:users] << user.username }
        approval_rule.groups.each { |group| approvers[:groups] << group.full_path }
      end
    end

    def approval_rules
      Triage.api_client.get("/projects/#{project_id}/merge_requests/#{merge_request_iid}/approval_rules").lazy
    end
  end
end
