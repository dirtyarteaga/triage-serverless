# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../triage/percentage_rollout'
require_relative '../triage/unique_comment'

module Triage
  class NewPipelineOnApproval < Processor
    DOC_FILE_REGEX = %r{\Adocs?/}
    include PercentageRollout

    # Merge request event can contain either `approved` or `approval` action
    # depending on the approval rules set for the merge request.
    # In this case, the reaction applies to either action.
    react_to 'merge_request.approval', 'merge_request.approved'
    percentage_rollout 100, on: ->(event) { event.iid.to_s }

    GITLAB_PROJECT_PATH = 'gitlab-org/gitlab'
    UPDATE_GITALY_BRANCH = 'release-tools/update-gitaly'
    STABLE_BRANCH_SUFFIX = 'stable-ee'

    def applicable?
      event.from_gitlab_org? &&
        from_gitlab_project?(event) &&
        need_new_pipeline?(event) &&
        unique_comment.no_previous_comment? &&
        !merge_request_changes_only_doc?
    end

    def process
      message = unique_comment.wrap(NewPipelineMessage.new(event))

      if event.gitlab_org_author?
        trigger_merge_request_pipeline
        add_comment(message)
      else
        add_discussion(message)
      end
    end

    private

    def unique_comment
      @unique_comment ||= UniqueComment.new(self.class.name, event)
    end

    def merge_request_changes
      Triage.api_client.merge_request_changes(project_id, merge_request_iid).changes
    end

    def project_id
      event.project_id
    end

    def merge_request_iid
      event.iid
    end

    def doc_change?(change)
      %w[old_path new_path].all? do |path|
        change[path].match?(DOC_FILE_REGEX)
      end
    end

    def merge_request_changes_only_doc?
      merge_request_changes.all? do |change|
        doc_change?(change)
      end
    end

    def trigger_merge_request_pipeline
      path = "#{event.noteable_path}/pipelines"

      Triage.api_client.post(path)
    end

    def from_gitlab_project?(event)
      event.project_path_with_namespace == GITLAB_PROJECT_PATH
    end

    def need_new_pipeline?(event)
      !(gitaly_update_branch?(event) || stable_branch?(event))
    end

    def gitaly_update_branch?(event)
      event.payload.dig('object_attributes', 'source_branch') == UPDATE_GITALY_BRANCH
    end

    def stable_branch?(event)
      event.payload.dig('object_attributes', 'target_branch').end_with?(STABLE_BRANCH_SUFFIX)
    end

    NewPipelineMessage = Struct.new(:event) do
      def to_s
        <<~MARKDOWN.chomp
          #{header}

          #{body}

          #{references}
        MARKDOWN
      end

      def header
        <<~HEADER.chomp
        :wave: `@#{event.user_username}`, thanks for approving this merge request.
        HEADER
      end

      def body
        <<~BODY.chomp
          This is the first time the merge request is approved. #{call_to_action}
        BODY
      end

      def references
        <<~REF.chomp
        For more info, please refer to the following links:
        - [`rspec` minimal jobs](https://docs.gitlab.com/ee/development/pipelines.html#rspec-minimal-jobs)
        - [`jest` minimal jobs](https://docs.gitlab.com/ee/development/pipelines.html#jest-minimal-jobs)
        - [merging a merge request](https://docs.gitlab.com/ee/development/code_review.html#merging-a-merge-request).
        REF
      end

      def call_to_action
        if event.gitlab_org_author?
          'To ensure full test coverage, a new pipeline has been started.'
        else
          'To ensure full test coverage, please start a new pipeline before merging.'
        end
      end
    end
  end
end
