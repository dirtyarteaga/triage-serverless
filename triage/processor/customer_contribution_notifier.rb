# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../triage/org_by_username_locator'

require 'slack-messenger'

module Triage
  class CustomerContributionNotifier < Processor
    CUSTOMER_PORTAL_URL = 'https://gitlab.my.salesforce.com/'
    GITLAB_INSTANCE_URL = 'https://gitlab.com/'
    DEFAULT_SLACK_CHANNEL = '#mrarr-wins'
    SLACK_ICON = ':robot_face:'
    DEFAULT_SLACK_OPTIONS = {
      channel: DEFAULT_SLACK_CHANNEL,
      username: GITLAB_BOT,
      icon_emoji: SLACK_ICON
    }.freeze
    CUSTOMER_CONTRIBUTION_MESSAGE_TEMPLATE = <<~MESSAGE
      > Organization: #{CUSTOMER_PORTAL_URL}%<org_name>s
      > Contribution Type: %<contribution_type>s
      > MR Link: %<mr_url>s
    MESSAGE

    CONTRIBUTION_TYPE_LABELS = [
      'type::feature',
      'type::bug',
      'type::tooling'
    ].freeze

    def initialize(event, messenger: slack_messenger)
      super(event)
      @messenger = messenger
    end

    def customer_contribution_message_template
      CUSTOMER_CONTRIBUTION_MESSAGE_TEMPLATE
    end

    def applicable?
      event.from_gitlab_org? &&
      community_contribution? &&
      contribution_from_customer_author?
    end

    def process
      post_message_to_slack
    end

    def slack_options
      DEFAULT_SLACK_OPTIONS.merge(channel: slack_channel)
    end

    def slack_channel
      DEFAULT_SLACK_CHANNEL
    end

    private

    attr_reader :messenger

    def community_contribution?
      event.wider_community_author?
    end

    def contribution_from_customer_author?
      !!org_name
    end

    def post_message_to_slack
      messenger.ping(formatted_message)
    end

    def formatted_message
      @formatted_message ||= format(customer_contribution_message_template,
        org_name: org_name,
        contribution_type: contribution_type,
        mr_url: mr_url)
    end

    def org_name
      @org_name ||= OrgByUsernameLocator.locate_org(event.author_id)
    end

    def contribution_type
      @contribution_type ||= CONTRIBUTION_TYPE_LABELS.find { |label_name| event.label_names.include?(label_name) }
    end

    def mr_url
      event.url
    end

    def slack_messenger
      Slack::Messenger.new(ENV['SLACK_WEBHOOK_URL'], slack_options)
    end
  end
end
