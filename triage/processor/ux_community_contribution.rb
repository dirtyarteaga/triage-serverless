# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../triage/unique_comment'

require 'digest'
require 'slack-messenger'

module Triage
  class UxCommunityContribution < Processor
    UX_LABEL = 'UX'
    SLACK_CHANNEL = '#ux-community-contributions'
    SLACK_ICON = ':robot_face:'
    SLACK_MESSAGE_TEMPLATE = <<~MESSAGE
      Hi UX team, a new community contribution (%<mr_title>s) requires a UX review: %<mr_url>s.
    MESSAGE

    react_to 'merge_request.open', 'merge_request.update'

    def initialize(event, messenger: slack_messenger)
      super(event)
      @messenger = messenger
    end

    def applicable?
      event.from_gitlab_org? &&
        event.resource_open? &&
        !event.wip? &&
        community_contribution? &&
        ux_label_added? &&
        unique_comment.no_previous_comment?
    end

    def process
      send_review_request
      post_ux_comment
    end

    def slack_options
      {
        channel: SLACK_CHANNEL,
        username: GITLAB_BOT,
        icon_emoji: SLACK_ICON
      }
    end

    private

    attr_reader :messenger

    def community_contribution?
      event.wider_community_author?
    end

    def ux_label_added?
      event.added_label_names.include?(UX_LABEL)
    end

    def send_review_request
      slack_message = format(SLACK_MESSAGE_TEMPLATE, mr_url: event.url, mr_title: event.title)
      messenger.ping(slack_message)
    end

    def unique_comment
      @unique_comment ||= UniqueComment.new(self.class.name, event)
    end

    def post_ux_comment
      add_comment(message.strip)
    end

    def message
      comment = <<~MESSAGE
        Thanks for helping us improve the UX of GitLab, your contribution is appreciated! We have pinged UX team members, stay tuned for their feedback.
      MESSAGE

      unique_comment.wrap(comment)
    end

    def slack_messenger
      Slack::Messenger.new(ENV['SLACK_WEBHOOK_URL'], slack_options)
    end
  end
end
