# Writing processor

Here is an example for a processor that posts a simple thank you message
when a merge request is created:

```ruby
module Triage
  class ThankYou < Triager
    react_to 'merge_request.open'

    def process
      add_comment('Thank you for the contribution!')
    end
  end
end
```

`react_to` can listen on various events, and react upon them. The format is
`RESOURCE.ACTION` where `RESOURCE` can be:

* `issue`
* `merge_request`
* `*` (means all of above)

And where `ACTION` can be different for different resources.

## Current list of actions for `issue`

* `open`
* `update`
* `note`
* `close`
* `reopen`
* `*` (means all of above)

## Current list of actions for `merge_request`

* `open`
* `update`
* `note`
* `close`
* `reopen`
* `approval`
* `unapproval`
* `approved`
* `unapproved`
* `merge`
* `*` (means all of above)
