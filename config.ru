# frozen_string_literal: true

require 'sucker_punch'

require_relative 'triage/rack/authenticator'
require_relative 'triage/rack/error_handler'
require_relative 'triage/rack/processor'
require_relative 'triage/rack/webhook_event'
require_relative 'triage/job/keep_cache_warm_job'

SuckerPunch.logger = Logger.new($stdout)
SuckerPunch.exception_handler = -> (error, klass, args) do
  Raven.tags_context(process: :sucker_punch)
  Raven.extra_context(job_class: klass, job_args: args)
  Raven.capture_exception(error)

  puts "SuckerPunch error for job #{klass.inspect} with args #{args}, error: #{error.class}: #{error.message}"
end

# Warm the cache immediately
Triage::KeepCacheWarmJob.perform_async

use Rack::CommonLogger, $stdout
use Rack::ContentType, 'application/json'

use Triage::Rack::ErrorHandler
use Triage::Rack::Authenticator
use Triage::Rack::WebhookEvent
run Triage::Rack::Processor.new
